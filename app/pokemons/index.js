import angular from 'angular'
import uirouter from 'angular-ui-router'
import { react2angular } from 'react2angular'
import routes from './pokemons.routes.js'
import pokemonList from './pokemon-list/pokemon-list.component'
import angularComponent from './angular-component/angular-component.component'
import PokemonsService from './pokemons.service'
import MyComponent from './react-angular-component/react-angular-component'

export default angular.module('pokemonPoc.pokemons', [uirouter])
  .config(routes)
  .component('pokemonList', pokemonList)
  .component('angularComponent', angularComponent)
  .component('myComponent', react2angular(MyComponent, ['imageUrl']))
  .service('PokemonsService', PokemonsService)
  .name
