import React from 'react';

class MyComponent extends React.Component {
  render() {
    return <div>
      <img alt="mug shot" src={this.props.imageUrl} />
    </div>
  }
}

export default MyComponent;