module.exports = {
  template: require('./angular-component.html'),
  controller: angularComponentCtrl
}

function angularComponentCtrl() {
    const bucketlist = [{
        city: 'Venice',
        position: 3,
        sites: ['Grand Canal', 'Bridge of Sighs', 'Piazza San Marco'],
        img: 'http://www.public-domain-photos.com/free-stock-photos-4/flowers/yellow-rose-3.jpg',
      }, {
        city: 'Paris',
        position: 2,
        sites: ['Eiffel Tower', 'The Louvre', 'Notre-Dame de Paris'],
        img: 'http://www.public-domain-photos.com/free-stock-photos-4/flowers/yellow-rose-3.jpg',
      }, {
        city: 'Santorini',
        position: 1,
        sites: ['Imerovigli', 'Akrotiri', 'Santorini Arts Factory'],
        img: 'http://www.public-domain-photos.com/free-stock-photos-4/flowers/yellow-rose-3.jpg',
      }];
    var ctrl = this;
    ctrl.bucketlist = bucketlist;
};